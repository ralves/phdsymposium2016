# -*- coding: utf-8 -*-

import os
import sys
import errno
import shutil
import json
import sass
from configparser import ConfigParser
import codecs
import datetime
import pyexcel_ods as pe
from glob import glob
from collections import OrderedDict
from subprocess import Popen, PIPE
from jinja2 import Environment, FileSystemLoader
from jsmin import jsmin

TEMPLATE_DIR = "templates/"
COMPILED_DIR = "compiled/"
ASSETS_DIR = "../assets/"
MAX_JS_LINE_LENGTH = "160"
COMPILED_FILES = set()


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise


def newer(first, second, ignore_timestamp=False):
    """Returns True if the first file is newer than the second
    If ignore_timestamp=True this function always returns True but keeps track
    of the compiled file.
    """

    def warn_recent():
        print("Updating", second, "with contents from", first)

    if not os.path.isfile(first):
        raise OSError("{} doesn't exist".format(first))

    # Keep track of compiled files even if they are not newer
    COMPILED_FILES.add(second)

    if ignore_timestamp:
        warn_recent()
        return True

    if not os.path.isfile(second):
        # If second file doesn't exist, always create it
        warn_recent()
        return True

    if os.stat(first).st_mtime > os.stat(second).st_mtime:
        warn_recent()
        return True

    return False


def update_thumb(first, second):
    """Uses ImageMagick to create thumbnails for speaker images"""

    # Ensure folder where second will be created exists
    second_dir = os.path.dirname(second)

    if not os.path.isdir(second_dir):
        mkdir_p(second_dir)

    p = Popen(["convert", "-resize", "80x96", "-format", "png", first, second])
    out, err = p.communicate()

    if p.returncode != 0:
        print("STDOUT:", out)
        print("STDERR:", err)

        raise OSError("Errors while generating thumbnail of", first)


def get_speakers():
    speaker_files = sorted(glob(os.path.join(TEMPLATE_DIR, "includes", "speakers", "*.txt")))
    speakers = []

    basepath = os.path.join("static", "img", "speakers")

    for speaker in speaker_files:
        c = ConfigParser()
        c.read_file(codecs.open(speaker, 'r', "utf8"))

        photo = os.path.join(basepath, c.get("data", "photo"))
        thumb = os.path.join(basepath, "thumb_{}".format(c.get("data", "photo")))

        photo_file = os.path.join(TEMPLATE_DIR, photo)
        thumb_file = os.path.join(COMPILED_DIR, thumb)

        if not os.path.isfile(photo_file):
            raise Exception("Photo file for {} is missing".format(speaker))

        if newer(photo_file, thumb_file):
            update_thumb(photo_file, thumb_file)

        speakers.append({
            "title": c.get("data", "title"),
            "name": c.get("data", "name"),
            "href": c.get("data", "website"),
            "inst": c.get("data", "institution"),
            "type": c.get("data", "type"),
            "high": c.get("data", "highlight"),
            "text": c.get("data", "about"),
            "time": c.get("data", "time"),
            "photo": photo,
            "thumb": thumb,
        })

    return speakers


def get_bus_stops(header):
    stops = []

    stops_index = 0
    for i, name in enumerate(header):
        if stops_index:
            stops.append(name)

        if name == "Stops":
            stops_index = i + 1

    return stops


def get_bus_timetable():
    """Read the bus schedule information from the ODS file"""

    schedule_file = os.path.join(ASSETS_DIR, "buses.ods")
    data = pe.get_data(schedule_file)

    days = OrderedDict()
    buses = []

    for sheet in data:
        if sheet == "Buses":
            terms = data[sheet][0]

            for row in data[sheet][1:]:
                bus = OrderedDict()
                for term, item in zip(terms, row):
                    if term == "BusType":
                        bus[term.lower()] = str(item).lower()
                    elif term == "Map":
                        bus[term.lower()] = os.path.join("static", "doc", item) if item else item
                    else:
                        bus[term.lower()] = item

                buses.append(bus)

            continue

        header = data[sheet][0]
        stops = get_bus_stops(header)

        days[sheet] = OrderedDict()

        for i, row in enumerate(data[sheet][1:]):
            trip = OrderedDict(zip(header, row))

            direction = trip["Direction"]

            if direction == "<":
                bus_stops = stops[::-1]
            else:
                bus_stops = stops[:]

            try:
                days[sheet][direction]["schedule"]
            except KeyError:
                bus = {}

                for item in ("From", "To"):
                    bus[item.lower()] = trip[item]

                bus["direction"] = direction
                bus["day"] = sheet
                bus["stops"] = bus_stops
                bus["schedule"] = []
            else:
                bus = days[sheet][direction]

            times = OrderedDict()

            for stop in bus_stops:
                times[stop] = [trip[stop]]

            first = None
            last = None

            # Add CSS style to stops according to their connection and first/last stop status
            # Requires 2 passes through the data
            for i, time in enumerate(times.values()):
                if time[0]:
                    if first is None:
                        first = i
                        continue

                    last = i

            if first is None or last is None:
                raise Exception("Each bus record needs at least two stops")

            for i, time in enumerate(times.values()):
                if i == first:
                    time.append("bus start")
                elif i > first and i < last:
                    if time[0]:
                        time.append("bus stop")
                    else:
                        time.append("bus")
                elif i == last:
                    time.append("bus end")
                else:
                    time.append("")

            bus_type = str(trip["BusType"]).lower()
            bus["schedule"].append((bus_type, times))

            days[sheet][direction] = bus

        for day in days:
            for direction in days[day]:
                schedule = days[day][direction]["schedule"]
                schedule.sort(key=lambda x: x[1]["EMBL"])

    return days, buses


def endswith(val, v):
    if str(val).endswith(str(v)):
        return True

    return False


def format_bus_time(time):
    try:
        return time.strftime("%H:%M")
    except AttributeError:
        return time


def main():
    # Change to website directory
    os.chdir("website")

    if not os.path.isdir(COMPILED_DIR):
        mkdir_p(COMPILED_DIR)

    with open(os.path.join(TEMPLATE_DIR, "variables.json")) as fh:
        context = json.load(fh)

    env = Environment(loader=FileSystemLoader(TEMPLATE_DIR))
    # Add custom endswith function to environment
    env.tests["endswith"] = endswith
    env.filters["bus_time"] = format_bus_time

    # Collect info on speakers and compile thumbnails
    context["speakers"] = get_speakers()
    timetable, buses = get_bus_timetable()
    context["bus_timetable"] = timetable
    context["buses"] = buses

    if datetime.datetime.strptime(context["FEEDBACKDAY"], "%d/%m/%Y") < datetime.datetime.now():
        context["FEEDBACK"] = True

    # Compile Jinja templates to HTML
    for file in context["COMPILE"]:
        template = env.get_template(file)
        if file.endswith(".jinja"):
            final_file = os.path.splitext(file)[0] + ".html"
            # The code that tracks if the template was modified only looks at
            # the main template and not all its hierarchy of dependencies.
            # Meaning it would only compile if the main template changes.
            ignore_timestamp = True
        else:
            final_file = file
            ignore_timestamp = False

        original_file = template.filename
        new_file = os.path.join(COMPILED_DIR, final_file)

        folder = os.path.dirname(new_file)
        if not os.path.isdir(folder):
            os.makedirs(folder)

        if newer(original_file, new_file, ignore_timestamp=ignore_timestamp):
            with open(new_file, 'wt') as fh:
                fh.write(template.render(context))

    # Copy files in static folder
    for root, dir, files in os.walk(os.path.join(TEMPLATE_DIR, "static")):
        for filename in files:
            # Ignore ".keep" files
            if filename == ".keep":
                continue

            destination = root.replace(TEMPLATE_DIR, COMPILED_DIR, 1)
            if not os.path.isdir(destination):
                mkdir_p(destination)

            original_file = os.path.join(root, filename)

            if filename.endswith(".sass"):
                name, ext = os.path.splitext(filename)
                new_file = os.path.join(destination, name + ".css")

                if newer(original_file, new_file):
                    with open(original_file) as fh, open(new_file, 'wt') as out:
                        out.write(sass.compile(string=fh.read()))

            elif filename.endswith(".js"):
                if not filename.endswith(".min.js"):
                    if sys.version_info.major == 2:
                        # Validate Javascript with Google Closure Linter
                        # if on Python 2 given closure-linter isn't py3 compatible
                        p = Popen(("gjslint", "--max_line_length", MAX_JS_LINE_LENGTH,
                                   original_file), stdout=PIPE, stderr=PIPE)
                        out, err = p.communicate()

                        if p.returncode != 0:
                            print("Errors while processing", original_file, ":")
                            print("STDOUT:", out.decode("utf8"))
                            print("STDERR:", err.decode("utf8"))

                            return 1

                    # For minification
                    name, _ = os.path.splitext(filename)
                    name += ".min.js"

                else:
                    # If file is already minified, keep the entire name
                    name = filename

                new_file = os.path.join(destination, name)

                if newer(original_file, new_file):
                    with open(original_file) as fh, open(new_file, 'wt') as out:
                        out.write(jsmin(fh.read()))

            else:
                new_file = os.path.join(destination,
                                        os.path.basename(original_file))

                if newer(original_file, new_file):
                    shutil.copy(original_file, destination)

    # Cleanup any file that was not part of the current compilation round
    for root, dir, files in os.walk(COMPILED_DIR):
        for filename in files:
            file = os.path.join(root, filename)
            if os.path.isfile(file) and file not in COMPILED_FILES:
                print("Removing", file)
                os.remove(file)

    return 0


if __name__ == "__main__":
    sys.exit(main())

# vim: ai sts=4 et sw=4
