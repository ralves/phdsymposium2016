[data]
title: Prof.
name: Paola Picotti
website: http://www.bc.biol.ethz.ch/research/picotti.html
time: Friday 18<sup>th</sup> - 16:15-17:00
institution: Department of Biochemistry, ETH Zürich - Switzerland
highlight: EMBO Young Investigator
type: lecture
about: Prof. Paola Picotti has obtained her PhD from the University of Padova. In 2006, she joined Prof. Ruedi Aebersold’s group at ETH Zurich, where her research received the ETH Latsis Prize 2011. At the beginning of 2011, she started her own research group and was appointed assistant professor at the Institute of Biochemistry at ETH Zurich. Her team focuses on the study of the effects of intracellular protein misfolding and aggregation using novel proteomics approaches. Among her pioneering achievements is the development of a method to analyze protein structural changes in biological samples and on large scale. In 2015 she was selected as an EMBO Young Investigator, while recently she won the 2016 Robert J. Cotter new investigator award for her significant contribution in the field of proteomics.
photo: paola_picotti.png
