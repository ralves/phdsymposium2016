[data]
title: Prof.
name: Timothy Ryan
website: http://vivo.med.cornell.edu/display/cwid-taryan
time: Saturday 19<sup>th</sup> - 15:15-16:00
institution: Weill Cornell Medical College - USA
highlight:
type: lecture
about: Prof. Ryan received his Bachelor of Science in Physics at McGill University and his PhD in Physics at Cornell in the laboratory of Watt Webb. After carrying out postdoctoral work at Stanford Medical School he started his own Weill Cornell Medical College, USA where he is currently a Tri-Institutional Professor. The focus of his lab is on the molecular basis of synaptic transmission in mammalian brain. His prime interest lies in understanding the regulation of presynaptic strength. Dr Ryan's group uses biophysical tools to measure physiological parameters at synapses, including exocytosis, endocytosis, action potential waveforms and voltage-triggered calcium fluxes in individual presynaptic boutons. Dr Ryan's team develops state-of-the-art optical methods to obtain a quantitative understanding of presynaptic function and attempts to reduce the complexities of synaptic transmission in a physico-chemical framework.
photo: timothy_ryan.png
