[data]
title: Prof.
name: Dagmar Iber
website: http://www.bsse.ethz.ch/cobi/
time: Saturday 19<sup>th</sup> - 10:00-10:45
institution: Department of Biosystems Science and Engineering, ETH Zürich - Switzerland
highlight:
type: lecture
about: Dagmar Iber studied mathematics and biochemistry in Regensburg, Cambridge and Oxford. She holds Master degrees and PhDs in both disciplines. After three years as a Junior Research Fellow in St John’s College, Oxford Dagmar became a lecturer in Applied Mathematics at Imperial College London. Dagmar has joined ETH Zuerich in 2008 after returning from an investment bank where she worked as an oil option trader for one year. Prof Iber’s group develops data-based, predictive models to understand the spatio-temporal dynamics of signaling networks. Her recent work focuses on mouse organogenesis and patterning systems to further understand the control of organ growth and robustness of signalling mechanisms in response to evolutionary changes.
photo: dagmar_iber.png
