[data]
title: Mr.
name: John Liebler
website: http://www.artofthecell.com/
time: Friday 18<sup>th</sup> - 13:45-14:30
institution: Founder of "Art of the Cell", Director of 3D Animation.
highlight:
type: society
about: As a former Lead Medical Animator of XVIVO Scientific Animation, John Liebler is best known for his work with Harvard University/Biovisions on the pivotal molecular movie “The Inner Life of the Cell”. He has over 20 years of experience creating scientific images and interactive 3D videos for a wide range of pharmaceutical, biomedical and educational companies. From science animation videos used to explain cell biology to students to scientific method animations clarifying the research of pioneers in the field of biosciences. John’s biomedical animations educate and inspire audiences of all backgrounds and interests across the globe.
photo: john_liebler.png
