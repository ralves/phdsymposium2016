$(document).ready(function() {
  var LbN = {
    // Mutate conference title letters to numbers
    register_mutate_number_title: function(target, toggle) {
      function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
      }

      function mutate(target, toggle) {
        var next = '';

        if (target.text() === toggle[0]) {
          next = toggle[1];
        } else {
          next = toggle[0];
        }

        target.fadeOut(500, function() {
          $(this).text(next).fadeIn(500);
        });

        kickstart_timer(target, toggle);
      }

      function kickstart_timer(target, toggle) {
        setTimeout(function() {
          mutate(target, toggle);
        }, getRandomInt(2000, 15000));
      }

      kickstart_timer($(target), toggle);
    },

    // Highlight the navigation menu according to the section currently in
    // focus. Uses js_nav_t and js_nav_ id's
    // js_nav_t are the targets to jump to
    register_highlight_current_menu_position: function() {
      var index = 0;
      var offsets = [];
      var current_offset = $(document).scrollTop();
      var menu_targets = $('div.js_nav_t');

      menu_targets.each(function(i) {
        var t = $(menu_targets[i]);
        offsets.push(t.offset().top + t.height());
      });

      for (index = 0; index < offsets.length; index++) {
        if (current_offset - offsets[index] < -200) {
          break;
        }
      }

      var id = $('#js_nav_'.concat(menu_targets[index].id));

      // Unmark active any active menu and mark as active the new match
      if (id.length !== 0) {
        var targets = $('a[id^="js_nav_"]');
        targets.removeClass('active');
        targets.parent().removeClass('active');

        id.addClass('active');
        id.parent().addClass('active');
      }
    },

    // Animate scrolling on page after clicking on the navigation menu
    register_scroll_menu_click: function() {
      $('a[href*="#"]').click(function(e) {
        var href = $.attr(this, 'href');

        href = href.substr(href.indexOf('#'));
        history.pushState(null, null, href);

        $('html, body').animate({
          scrollTop: $(href).offset().top
        }, 300);

        return false;
      });
    },

    // Animate transition of prgramme days
    transition_day: function(direction) {
      var current = $('#programme_wrap .day.current');

      if (direction === 'next') {
        var next = current.next();
      } else {
        var next = current.prev();
      }

      if (next.size()) {
        // Mark active element
        current.removeClass('current');
        next.addClass('current');

        // Trigger the animation
        if (direction === 'next') {
          current.addClass('left');
          next.removeClass('right');
        } else {
          current.addClass('right');
          next.removeClass('left');
        }
      }
    },

    update_active_speaker: function(direction, target) {
      // Remove the currently active thumbnail highlight
      $('.speaker_thumb').parent().removeClass('active');

      var target_id = 0;

      if (typeof target != 'undefined') {
        // highlight the thumbnail of the currently visible speaker
        // it should always be the second item in the speakers list
        target_id = target.attr('id');
      }
      // highlight the thumbnail of the currently visible speaker
      // it should always be the second item in the speakers list
      $('#thumb_' + target_id).parent().addClass('active');
    },

    register_transition_speaker: function() {
      function resetSpeakers() {
        // and adjust the container so current is in the frame
        container.css({
          'left': -1 * item_width
        });
      };

      //rotation speed
      var speed = 500;

      var speakers = $('.speaker');
      var container = $('#speakers_container');
      var item_width = container.width();

      //set the slides to the correct pixel width
      speakers.width(item_width);
      container.parent().width(item_width);
      //set the slides container to the correct total width
      container.width(speakers.length * item_width);

      // Register clicks on speaker's thumbnails to trigger a jump
      $('.speaker_link').click(function(e) {
        e.preventDefault();

        var elm = $(this).children('.speaker_thumb');
        var target = $(elm.attr('id').replace(/thumb_/, '#'));

        LbN.transition_speaker('next', target);
      });

      LbN.transition_speaker = function(direction, target) {
        if (container.is(':animated')) {
          return false;
        }

        if (typeof target == 'undefined') {
          var thumb_active = $('div#smallspeakers .active img:first-child').attr('id');
          var id = $(thumb_active.replace(/thumb_/, '#'));
          var all_thumbs = $('div#smallspeakers img:first-child');
          var first = $(all_thumbs.first().attr('id').replace(/thumb_/, '#'));
          var last = $(all_thumbs.last().attr('id').replace(/thumb_/, '#'));

          if (direction === 'next') {
            target = id.next();

            if (target.length === 0) {
              target = first;
            }

          } else {
            target = id.prev();

            if (target.length === 0) {
              target = last;
            }
          }
        }

        // We have a target to jump to do the math and jump to it
        var i = speakers.index(target);

        // How many jumps to perform to the right or left
        var jumpfactor = i / 2;

        // Update the next active speaker
        LbN.update_active_speaker(direction, target);

        container.stop().animate({
          'left': item_width * -2 * jumpfactor
        }, speed, function() {
          // This was disabled as the animation was annoying on large screens
          // $('html, body').animate({
          //   scrollTop: $('#speakers div h1').offset().top
          // }, 500);
        });

        //cancel the link behavior
        return false;
      };
    },

    speakers_play: function() {
      var twomins = 1000 * 60 * 2;

      LbN.speakers_play_handle = setTimeout(function() {
        LbN.transition_speaker('next');
        LbN.speakers_play();
      }, twomins);
    },

    resize_video: function(tag_id) {
      var $vid = $(tag_id);
      var resize_factor = 0.7;
      var $window = $(window);

      var aspect_ratio = $vid.attr('data-width') / $vid.attr('data-height');
      // Adjust youtube video dimensions
      if ($window.width() * resize_factor < $vid.attr('data-width')) {
        $vid.attr('width', $window.width() * resize_factor);
        $vid.attr('height', $vid.attr('width') / aspect_ratio);
      }
    },
  };

  // When page is scrolled update menu selection
  $(document).ready(function() {
    $(document).scroll(LbN.register_highlight_current_menu_position);

    // Start mutating the logo
    LbN.register_mutate_number_title('#title-1', ['1', 'i']);
    LbN.register_mutate_number_title('#title-3a', ['3', 'e']);
    LbN.register_mutate_number_title('#title-3b', ['3', 'e']);
    LbN.register_mutate_number_title('#title-5', ['5', 's']);

    // Run the function once to activate the menu on page visit
    LbN.register_highlight_current_menu_position();

    // Register smooth scroll on menu click
    LbN.register_scroll_menu_click();

    // Set the nav bar sticky
    $('#navmenu').sticky({
      topSpacing: 0,
      responsiveWidth: true,
      center: true,
      getWidthFrom: 'body',
      wrapperClassName: 'nav-wrapper'
    });
    // Force navmenu wrapper to use full width even if window is resized
    $('.nav-wrapper').css('width', '100%');

    // Prepare speakers sliding panel
    LbN.register_transition_speaker();

    // Move left/right in speakers or programme by clicking the arrows
    $('#rarr').click(function() {
      if ($('#js_nav_speakers').hasClass('active')) LbN.transition_speaker('next');
      if ($('#js_nav_programme').hasClass('active')) LbN.transition_day('next');
    });

    $('#larr').click(function() {
      if ($('#js_nav_speakers').hasClass('active')) LbN.transition_speaker('prev');
      if ($('#js_nav_programme').hasClass('active')) LbN.transition_day('prev');
    });

    // Navigation menu managing when displaying on small width screens
    // Show the expanded menu when nav is clicked
    $('nav').click(function() {
      $('nav').removeClass('show');
    });

    // Hide the expanded menu once an item is clicked
    $('nav ul li a').click(function(e) {
      $('nav').toggleClass('show');
    });

    // Modal flying window containing google map
    $('#mapButton').click(function(e) {
      e.preventDefault();
      $('#modalMap').show();
    });

    $('.modalClose').click(function() {
      $('#modalMap').hide();
    });

    $('#speakers_play').click(function() {
      if (! LbN.speakers_play_active) {
        LbN.speakers_play_active = true;
        LbN.speakers_play();
        $('#speakers_play h1').append('<span id="speakers_play_icon"> &#9658;</span>');
      } else {
        LbN.speakers_play_active = false;
        clearTimeout(LbN.speakers_play_handle);
        $('#speakers_play_icon').remove();
      }
    });

    var $window = $(window);

    $window.click(function(event) {
      var modalmap = $('#modalMap');
      if (event.target == modalmap[0]) {
        modalmap.hide();
      }
    });

    // Resize once at load time
    LbN.resize_video('#youtubevid');

    // And in the future if window size changes
    $window.resize(function() {
      LbN.resize_video('#youtubevid');
    });

  });
});
