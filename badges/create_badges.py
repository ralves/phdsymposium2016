# -*- coding: utf-8 -*-

from __future__ import division, print_function
from lxml import etree
import openpyxl


def get_values(row):
    return map(lambda x: x.value, row)


def replace_color(obj, orig, new):
    style = obj.get("style")

    if style is not None:
        style = style.replace(orig, new)
        obj.set("style", style)

    for tag in obj:
        replace_color(tag, orig, new)


BADGES_PER_FILE = 10
BADGE_FILE = "badges.svg"
BADGE_TEMPLATE = "badges_%i.svg"
FIRSTNAME_TAG = "{{FirstName%s}}"
LASTNAME_TAG = "{{LastName%s}}"
ORGANIZATION_TAG = "{{Organization%s}}"

EMBL_BADGE_COLOR = "902a00"
EMBL_TEXT_COLOR = "902a00"
EXTERNAL_BADGE_COLOR = "4d5781"
EXTERNAL_TEXT_COLOR = "2d396a"

FILENAME = "Sorted_file_V5-1.xlsx"

keep_rows = ["Badge", "Submitter Last", "Submitter First", "Submitter Company"]
row_ids = {}
participants = []

wb = openpyxl.load_workbook(filename=FILENAME, read_only=True)
ws = wb["Sorted for Karen"]

firstrow = get_values(next(ws.rows))

for i, cell in enumerate(firstrow):
    if cell in keep_rows:
        row_ids[i] = cell

for row in ws.rows:
    badge = {}

    for i, cell in enumerate(get_values(row)):
        if i in row_ids:
            badge[row_ids[i]] = cell

    if badge["Badge"] == "No":
        print("Skipping as requested:", badge)
        continue

    participants.append(badge)

embl = 0
nonembl = 0

for i in range(0, len(participants), BADGES_PER_FILE):
    with open(BADGE_FILE) as fh:
        data = fh.read()

    root = etree.fromstring(data)

    for j, part in enumerate(participants[i:i+BADGES_PER_FILE]):
        nonembl += 1
        first = FIRSTNAME_TAG % (j + 1)
        last = LASTNAME_TAG % (j + 1)
        org = ORGANIZATION_TAG % (j + 1)

        for node in root:
            if node.tag.endswith("}g"):  # There's two levels of groups, the layer...
                for obj in node:
                    if obj.tag.endswith("}g"):  # and each badge
                        for group in obj:
                            if group.tag.endswith("}text"):
                                for elem in group:
                                    if elem.text == first:
                                        elem.text = part["Submitter First"]
                                    elif elem.text == last:
                                        elem.text = part["Submitter Last"]
                                    elif elem.text == org:
                                        elem.text = part["Submitter Company"]
                                        if elem.text.startswith("EMBL"):
                                            embl += 1
                                            replace_color(obj, EXTERNAL_BADGE_COLOR, EMBL_BADGE_COLOR)
                                            replace_color(obj, EXTERNAL_TEXT_COLOR, EMBL_TEXT_COLOR)

    output_file = BADGE_TEMPLATE % (i / BADGES_PER_FILE)

    with open(output_file, 'w') as out:
        out.write(etree.tostring(root).encode("utf8"))

print("EMBL", embl)
print("Non EMBL", nonembl - embl)

# vim: ai sts=4 et sw=4
