.PHONY: install run setup test always

# This is aliased in ~/.ssh/config to bibo.embl.de
# Or some other server that has access to the PRODUCTION_DEST folder
PRODUCTION_HOST="symp2016"
PRODUCTION_DEST="/g/wwwl/phdsymposium.embl.org/htdocs/symp2016/"

WEBSITE_PARTS=$(wildcard website/compiled/*.html website/compiled/*/*.html)
WEBSITE="website/compiled/index.html"
# DEPENDENCIES="venv/bin/w3c-validator"
DEPENDENCIES="venv/bin/pysassc"

$(WEBSITE):
	@echo "*** Compiling website ... $@ "
	@. venv/bin/activate && python build.py
	@echo "*** Compiling website ... DONE"

$(WEBSITE_PARTS): $(WEBSITE)

venv:
	@echo "*** Setting up venv ... "
	@python -m venv venv
	@echo "*** Setting up venv ... DONE"

$(DEPENDENCIES): venv
	@echo "*** Installing dependencies locally ... "
	# @. venv/bin/activate && pip install --upgrade -r requirements.txt && patch -p0 < resources/w3c_html5_fix.patch
	@. venv/bin/activate && pip install --upgrade -r requirements.txt
	@echo "*** Installing dependencies locally ... DONE"

setup: $(DEPENDENCIES)

run: $(WEBSITE)
	@echo "*** Serving website on http://localhost:8000 ... "
	@. venv/bin/activate && cd website/compiled/ && python -m http.server 8000
	@echo "*** Serving website on http://localhost:8000 ... DONE"

test: $(WEBSITE_PARTS)
	@echo "*** Validating HTML generated ... "
	@. venv/bin/activate && w3c-validator $(WEBSITE_PARTS)
	@echo "*** Validating HTML generated ... DONE"

install: test
	@echo "*** Copying files to remote server ... "
	@rsync -rclDP --delete-delay website/compiled/ $(PRODUCTION_HOST):$(PRODUCTION_DEST)
	@echo "*** Copying files to remote server ... DONE"
	@echo "*** Fixing permissions on remote files ... "
	@ssh $(PRODUCTION_HOST) "find $(PRODUCTION_DEST) ! -path $(PRODUCTION_DEST) -type d -exec chmod 755 {} \\;"
	@ssh $(PRODUCTION_HOST) "find $(PRODUCTION_DEST) ! -path $(PRODUCTION_DEST) -type f -exec chmod 644 {} \\;"
	@echo "*** Fixing permissions on remote files ... DONE"

install-dev: test
	@echo "*** Copying files to remote server ... "
	@rsync -rclDP --delete-delay website/compiled/ $(PRODUCTION_HOST):$(PRODUCTION_DEST)dev/
	@echo "*** Copying files to remote server ... DONE"
	@echo "*** Fixing permissions on remote files ... "
	@ssh $(PRODUCTION_HOST) "find $(PRODUCTION_DEST) ! -path $(PRODUCTION_DEST) -type d -exec chmod 755 {} \\;"
	@ssh $(PRODUCTION_HOST) "find $(PRODUCTION_DEST) ! -path $(PRODUCTION_DEST) -type f -exec chmod 644 {} \\;"
	@echo "*** Fixing permissions on remote files ... DONE"

todo:
	@echo "*** Searching for TODOs in the code ... "
	@cd website/templates/ && git grep TODO
	@echo "*** Searching for TODOs in the code ... DONE"
