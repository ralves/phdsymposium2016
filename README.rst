Development instructions
========================

::

    make setup       - Configures a virtualenv with all necessary dependencies.
    make test        - Runs main page (index.html) against w3c validator.
    make install     - Deploys the website to the production server

Additionally you can also issue::

    make run         - Runs a simple HTTP local server for development.
    make install-dev - Deploy the website to the production server under a development folder.
    make todo        - Get a list of all TODO's on the current code

and to automatically build the website every time a file changes run::

    ./autohtmlbuilder.sh
